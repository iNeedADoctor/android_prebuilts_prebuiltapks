Fork of [lineageos4microg/android_vendor_partner_gms](https://github.com/lineageos4microg/android_vendor_partner_gms)

# Prebuilt APKs

This is a collection of APKs, coupled with the respective Android.mk for an
easy integration in the Android build system.

To include them in your build you need:
1. add the correct manifest file (see below)
1. add the name of application in CUSTOM_PACKAGES (for example in `vendor/lineage/config/common.mk`).


## Manifests

Manifest for LineageOS **14.1** and **15.1**:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <remote name="codeberg" fetch="https://codeberg.org"/>

    <projec name="iNeedADoctor/android_prebuilts_prebuiltapks"t path="prebuilts/prebuiltapks" remote="codeberg" revision="lineage-14.1" />
</manifest>
```

Manifest for LineageOS \=\> **16.0** and \<\= **18.1**:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <remote name="codeberg" fetch="https://codeberg.org"/>

    <project name="iNeedADoctor/android_prebuilts_prebuiltapks" path="prebuilts/prebuiltapks" remote="codeberg" revision="lineage-16.0" />
</manifest>
```

Manifest for LineageOS **19.1**:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <remote name="codeberg" fetch="https://codeberg.org"/>

    <project name="iNeedADoctor/android_prebuilts_prebuiltapks" path="prebuilts/prebuiltapks" remote="codeberg" revision="lineage-19.0" />
</manifest>
```

Manifest for LineageOS **20.0**:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <remote name="codeberg" fetch="https://codeberg.org"/>

    <project name="iNeedADoctor/android_prebuilts_prebuiltapks" path="prebuilts/prebuiltapks" remote="codeberg" revision="lineage-20.0" />
</manifest>
```

## Applications

The included open source APKs are:
 * FDroid packages (binaries sourced from [here](https://f-droid.org/packages/org.fdroid.fdroid/) and [here](https://f-droid.org/packages/org.fdroid.fdroid.privileged/)):
   * FDroid: a catalogue of FOSS (Free and Open Source Software) applications for the Android platform
   * FDroidPrivilegedExtension: a FDroid extension to ease the installation/removal of apps
   * additional_repos.xml: a simple package to include the in the ROM the following repositories (requires FDroid >= 1.5):
     * [microG FDroid repository](https://microg.org/fdroid.html)
     * [NewPipe upstream repository](https://newpipe.net/FAQ/tutorials/install-add-fdroid-repo/)
     * [IzzyOnDroid F-Droid Repo](https://apt.izzysoft.de/fdroid/index.php)
     * [DivestOS Official](https://divestos.org/index.php?page=our_apps#repos)
     * [Molly F-Droid Repo](https://molly.im/download/fdroid/)
 	 * [Collabora Office](https://www.collaboraoffice.com/downloads/fdroid/repo)
	 * [FFUpdater](https://github.com/Tobi823/ffupdater/blob/master/docs/other_distribution_channels.md)
 * microG packages (binaries sourced from [here](https://microg.org/download.html)):
   * MicroGServices: the main component of microG, a FOSS reimplementation of the Google Play Services (requires GsfProxy and FakeStore for full functionality)
   * GsfProxy: a GmsCore proxy for legacy GCM compatibility
   * MicroGCompanion: an empty package that mocks the existence of the Google Play Store
   * IchnaeaNlpBackend: Network location provider using Mozilla Location Service
   * NominatimGeocoderBackend: Geocoder backend that uses OSM Nominatim service.
  * LocalGsmNlpBackend: location provider using local GSM database (OpenCellID/MozillaLocationServices/Radiocells.org) (binaries sourced from [here](https://f-droid.org/en/packages/org.fitchfamily.android.gsmlocation))
 * AuroraOSS packages (binaries sourced from [here](https://gitlab.com/AuroraOSS))
   * AuroraStore: alternative for Google Play Store
   * AuroraServices: service provider for Aurora Store
 * Etar: open source calendar (binaries sourced from [here](https://f-droid.org/packages/ws.xsoh.etar/))

The included close source APKs are:
 * MiXplorer packages (binaries sourced from [here](https://forum.xda-developers.com/t/app-2-2-mixplorer-v6-x-released-fully-featured-file-manager.1523691/#post-23109280))
   * MiXplorer: file manager
   * MiXArchive: MiXplorer addon
   * MiXPDF: MiXplorer addon
   * MiXImage: MiXplorer addon
   * MiXCodecs: MiXplorer addon
   * MiXTagger: MiXplorer addon
   * MiXAutoTag: MiXplorer addon

These are official unmodified prebuilt binaries, signed by the
corresponding developers.
